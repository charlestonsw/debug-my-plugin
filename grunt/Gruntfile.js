/**
 * A Grunt script that assists in building WordPress prodcution plugins.
 *
 * Make sure you update the package.json file to set the WordPress plugin directory path,
 * your author name, and to tell this script about the meta data for your plugins that are
 * to be managed here.
 *
 */

/*global module:false*/
module.exports = function(grunt) {
  var currentPlugin = {};
  var myServer = {};
  var pluginMetadata = grunt.file.readJSON('plugins.json');


  // Project configuration.
  grunt.initConfig({

    // Metadata.
    pluginMetadata: pluginMetadata,
    currentPlugin: currentPlugin,
    myServer: myServer,
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    my_plugin_dir: '/var/www/html/wp-content/plugins/<%= currentPlugin.slug %>',
    my_src_files: [
                    '**',
                    '!**.neon',
                    '!**.md',
                    '!assets/**',
                    '!nbproject/**'
                ],
    my_server_files: [
                    "plugins.json" ,
                    "<%= currentPlugin.zipbase %>.zip" ,
                    "<%= currentPlugin.zipbase %>_readme.txt"
                ],

    //--------------------
    // Task configuration.
    //--------------------

    // checktextdomain
    //
    checktextdomain: {
        standard: {
            options:{
                text_domain: '<%= currentPlugin.textdomain %>',
                keywords: [
                    '__:1,2d',
                    '_e:1,2d',
                    '_x:1,2c,3d',
                    'esc_html__:1,2d',
                    'esc_html_e:1,2d',
                    'esc_html_x:1,2c,3d',
                    'esc_attr__:1,2d',
                    'esc_attr_e:1,2d',
                    'esc_attr_x:1,2c,3d',
                    '_ex:1,2c,3d',
                    '_n:1,2,4d',
                    '_nx:1,2,4c,5d',
                    '_n_noop:1,2,3d',
                    '_nx_noop:1,2,3c,4d'
                ],
                correct_domain: true
            },
            files: [{
                cwd: '<%= my_plugin_dir %>',
                expand: true,
                src: ['**/*.php'],
            }]
        }
    },

    // checkwpversion
    //
    checkwpversion: {
        options: {
            readme: '<%= my_plugin_dir %>/readme.txt',
            plugin: '<%= my_plugin_dir %>/<%= currentPlugin.slug %>.php'
        },
        check: {
            version1: 'plugin',
            version2: 'readme',
            compare: '=='
        }
    },

    // compress
    //
    compress: {
        options: {
            mode: 'zip',
            archive: '../public/<%= grunt.task.current.target %>/<%= currentPlugin.zipbase %>.zip',
        },
        prerelease: { expand: true, cwd: '<%= my_plugin_dir %>', src: '<%= my_src_files %>', dest: '<%= currentPlugin.slug %>' },
        production: { expand: true, cwd: '<%= my_plugin_dir %>', src: '<%= my_src_files %>', dest: '<%= currentPlugin.slug %>' },
    },

    // concat
    //
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      dist: {
        src: ['lib/<%= pkg.name %>.js'],
        dest: 'dist/<%= pkg.name %>.js'
      }
    },

    // copy
    //
    copy: {
        metadata: {
            files: [
                {
                    expand: false,
                    src: '<%= my_plugin_dir %>/readme.txt',
                    dest: "../public/<%= currentPlugin.target %>/<%= currentPlugin.zipbase %>_readme.txt"
                },
                {
                    src: 'plugins.json',
                    dest: "../public/<%= currentPlugin.target %>/plugins.json"
                }
            ]
        },
        wp_trunk: {
            files: [
                {
                    expand: true,
                    cwd: '<%= my_plugin_dir %>',
                    src: '<%= my_src_files %>',
                    dest: '<%= my_plugin_dir %>/assets/build/'
                }
            ]
        }
    },

    // readbump
    //
    readbump: {
        prerelease: { expand: true, cwd: '<%= my_plugin_dir %>', src: 'readme.txt' },
        production: { expand: true, cwd: '<%= my_plugin_dir %>', src: 'readme.txt' },
    },

    // uglify
    //
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        src: '<%= concat.dist.dest %>',
        dest: 'dist/<%= pkg.name %>.min.js'
      }
    },

    // jshint
    //
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        globals: {}
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    
    // shell
    //
    shell: {
        resetdb: {
            command: function() {
                return "mysql --user vagrant --password=vagrant wordpress -e 'drop database wordpress; create database wordpress;'";
            }, 
            options: {
                silent: true
            }
        }
    },

    // sftp
    //
    sftp: {
        options: {
          host: '<%= myServer.host %>',
          username: '<%= myServer.username %>',
          privateKey: '<%= myServer.privateKey %>',
          passphrase: '<%= myServer.passphrase %>',
          path: '<%= myServer.path %><%= grunt.task.current.target %>/',
          srcBasePath: "../public/<%= grunt.task.current.target %>/",
          showProgress: true
        }, 
        production: { expand: true, cwd: "../public/<%= grunt.task.current.target %>/", src: "<%= my_server_files %>" },
        prerelease: { expand: true, cwd: "../public/<%= grunt.task.current.target %>/", src: "<%= my_server_files %>" }
    },

    // watch
    //
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'nodeunit']
      }
    } ,


    // wp_deploy
    //
    wp_deploy: {
        deploy: {
            options: {
                plugin_slug: '<%= currentPlugin.slug %>',
                svn_user: '<%= currentPlugin.username %>',
                svn_url: 'http://plugins.svn.wordpress.org/<%= currentPlugin.reposlug %>',
                build_dir: '<%= my_plugin_dir %>/assets/build/'
            },
        }
    }

  });

  //---------------------------------------
  // These plugins provide necessary tasks.
  //---------------------------------------
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-ssh');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-checkwpversion');
  grunt.loadNpmTasks('grunt-wp-deploy');
  grunt.loadNpmTasks('grunt-checktextdomain');

    //-----------------
    // Helper Functions
    //-----------------

    /**
     * Set the currentPlugin properties.
     * @param string slug
     * @returns null
     */
    function get_plugin_settings(slug) {
      for(var propertyName in pluginMetadata.pluginMeta[slug]) {
          currentPlugin[propertyName] = pluginMetadata.pluginMeta[slug][propertyName];
      }
      currentPlugin.slug = slug;
      currentPlugin.username = pluginMetadata["wp-username"];                                   // Users WP directory publisher id
      currentPlugin.path = pluginMetadata["wp-plugin-dir"] + currentPlugin.slug + '/';          // Where the plugin lives
      currentPlugin.repo_path = currentPlugin.path + pluginMetadata["wp-default-repo-dir"];     // Where the plugin WP svn repo lives

      // Some defaults
      //
      if ( currentPlugin.reposlug == null ) {
          currentPlugin.reposlug = slug;
      }
      if ( currentPlugin.zipbase == null ) {
          currentPlugin.zipbase = slug;
      }
    }

  //-------------
  // Custom Tasks
  //-------------

  /**
   * build
   *
   * By default it builds the zip file in the public directory and leaves it there.
   *
   *
   * You can specify a second parameter to determine the target of build.  Valid targets are:
   * - production compresses and then pushes the file to a private server production folder or updates a WP hosted plugin
   * - prerelease (default) compresses and then pushes the file to a private server prerelease folder
   *
   * 'production' target:
   *  Where the file is distributed to is defined in plugins.json via the publish-to parameter.
   * 'myserver' or 'wordpress' are accepted.
   *
   * 'prerelease' target:
   * Expects myserver settings to be configured.
   *
   * @param string slug the plugin slug
   * @param string target what type of build are we doing?
   */
  grunt.registerTask('build', 'Build the specified plugin.  Plugin slug required build:slug.', function(slug,target) {

      // Slug Missing
      //
      if ( slug == null ) {
          if ( currentPlugin.slug == null ) {
            grunt.task.run('instructions');
            return;
          }

      // Set currentPlugin.slug to slug that was passed in
      //
      } else {
          if ( currentPlugin.slug == null ) {
              currentPlugin.slug = slug;
          }
      }
      get_plugin_settings(currentPlugin.slug);

      // target missing
      if ( target == null ) {
          target='prerelease';
      }

      // Compress (zipfile builder) is always run with a build.
      //
      var taskList = ['checkwpversion','checktextdomain','compress:'+target];

      // Setup The Build Tasks
      //
      grunt.log.writeln('Building ' + currentPlugin.slug + ' for ' + target);
      switch (target) {
          case 'prerelease':
            currentPlugin.publishto = 'myserver';

          case 'production':
            taskList.push('publish:'+currentPlugin.slug+':prerelease', 'publish:'+currentPlugin.slug+':'+target);
            break;

          default:
            grunt.log.writeln(target + ' is not a valid target. Try one of these: ');
            grunt.log.writeln('prerelease (default)');
            grunt.log.writeln('production');
            return;
      }

      // Build the plugin, always create the zip
      //
      grunt.task.run(taskList);
  });

  /**
   * details
   * 
   * List the details about each plugin.
   */
  grunt.registerTask('details', 'List plugin details.', function() {
      for (var key in pluginMetadata.pluginMeta) {
          get_plugin_settings(key);
          var version_string = ' production v' + currentPlugin.production.new_version;
          if ( typeof currentPlugin.prerelease !== 'undefined' ) {
              version_string += ' => prerelease v' + currentPlugin.prerelease.new_version;
          }
          grunt.log.writeln( currentPlugin.slug + version_string );
      }
  });

  /**
   * instructions
   *
   * Show the usage instructions for this Grunt script.
   */
  grunt.registerTask('instructions', 'Show usage instructions.', function() {
      grunt.log.writeln('Need to specify a plugin slug name to actually do something useful.');
      grunt.task.run('list');
  });

  /**
   * list
   *
   * List the available plugin slugs.
   */
  grunt.registerTask('list', 'List plugins that are supported by this kit.', function() {
      grunt.log.writeln('Available plugin slugs:');
      for (var key in pluginMetadata.pluginMeta) {
          grunt.log.writeln(key);
      }
  });

  /**
   * publish
   *
   * The plugin will be either copied with SFTP to a private server or unpacked into an
   * svn repository directory and pushed to what is assumed to be a WordPress Plugin Directory
   * repo.
   *
   * @param string slug
   * @param string target = the publication target
   */
  grunt.registerTask('publish', 'Publish the specified plugin.', function(slug,target) {
      grunt.log.writeln('publishing ' + slug + ' ' + target + ' to ' + currentPlugin.publishto);

      // Slug and currentPlugin.slug not set - exit
      //
      if ( slug == null ) {
          if ( currentPlugin.slug == null ) {
            grunt.task.run('instructions');
            return;
          }

      // Set currentPlugin.slug to slug that was passed in
      //
      } else {
          if ( currentPlugin.slug == null ) {
              currentPlugin.slug = slug;
          }
      }
      get_plugin_settings(currentPlugin.slug);

      // What publish type?
      //  Set to prerelease if not specified.
      //
      if ( target == null ) {
          target = 'prerelease';
      }

      // Setup for different targets
      //
      switch ( target ) {
        case 'prerelease':
            currentPlugin.publishto = 'myserver';
            break;
        default:
            break;
      }
      currentPlugin.target = target;

      // Publish to the destination
      //
      switch ( currentPlugin.publishto ) {

          // A private server location
          //
          case 'myserver':
                myServerObj = grunt.file.readJSON('myserver.json');
                myServer.host = myServerObj.host;
                myServer.username = myServerObj.username;
                myServer.path = myServerObj.path;
                myServer.privateKey = grunt.file.read(myServerObj.privateKeyFile);
                myServer.passphrase = grunt.file.read(myServerObj.privateKeyPassFile);
                grunt.task.run(['readbump:'+target,'copy:metadata','sftp:'+target]);
                break;

          // The public WP Plugin Directory
          //
          case 'wordpress':
                grunt.task.run(['readbump:'+target,'copy:metadata','sftp:'+target,'copy:wp_trunk','wp_deploy']);
                break;

          // Not a valid publishto setting
          //
          default:
                grunt.log.writeln(currentPlugin.publishto + ' is not a valid publishto target. Try one of these: ');
                grunt.log.writeln('myserver');
                grunt.log.writeln('wordpress');
                return;
        }

  });

  /**
   * readbump
   *
   * Fetch version from readme and bump date.
   */
  grunt.registerMultiTask('readbump', 'Fetch version from readme and bump date.', function() {
    var currentTarget = this.target;
    var starting_version = pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['new_version'];
    var starting_date    = pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['last_updated'];
    var readme_version = '';

    // README Processing
    // Loop all file lists
    //
    this.files.forEach(function(filelist) {

        // Process each individual file in the current file list
        filelist.src.forEach(function(filename){

            // Readme get version
            //
            var readme = grunt.file.read( filename );
            matches = readme.match( new RegExp("^Stable tag:\\s*(\\S+)","im") );
            if( matches.length <= 1 ){
                grunt.fail.fatal( 'Could not find version in "' + filename + '"' );
            }
            readme_version = matches[1];

            if ( readme_version !== starting_version ) {
                pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['new_version'] = readme_version;
            }
        })
    });

    // Set the date to today
    //
    var today = new Date();
    var formatted_date = today.getFullYear() + '-' + ('0'+(today.getMonth()+1)).slice(-2) + '-' + today.getDate();
    if ( pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['last_updated'] !== formatted_date ) {
        pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['last_updated'] = formatted_date;
    }

    // Any changes?
    //
    var version_changed = ( pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['new_version'] !== starting_version );
    var date_changed    = ( pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['last_updated']!== starting_date    );

    // Write updated plugins.json
    //
    if  ( version_changed || date_changed ) {
        grunt.file.write( 'plugins.json', JSON.stringify(pluginMetadata,null,4) );
        grunt.log.writeln(currentPlugin.slug + ' ' + currentTarget + ' updated in plugins.json');
        grunt.log.writeln(' version ' + pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['new_version'] );
        grunt.log.writeln(' updated ' + pluginMetadata.pluginMeta[currentPlugin.slug][currentTarget]['last_updated'] );
    } else {
        grunt.log.writeln('no changes were needed for plugins.json');
    }
  });
  

  // Turn off Grunt headers
  // grunt.log.header = function() { };

  // Default task.
  grunt.registerTask('default', 'instructions');

};
