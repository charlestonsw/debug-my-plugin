#!/bin/bash
if [[ -z "$PLUGIN_NAME" ]]
then
    if [[ $# < 1 ]]
    then
       read -p "Plugin Name: "  PLUGIN_NAME
    else
       PLUGIN_NAME=$1
    fi
fi
HOMEDIR=~
CDIR=`pwd`
ZIP_DIR="$HOMEDIR/myplugins"
LICMAN_DIR="$HOMEDIR/NetBeansProjects/csa_licman"
PLUGIN_DIR="$CDIR/$PLUGIN_NAME"
SVN_DIR="$PLUGIN_DIR/assets/svnrepo"
PUB_DIR="$PLUGIN_DIR/assets/public"
