#!/bin/bash

# This script runs the makezip script then runs the commands
# needed to update the WordPress repo other than the final
# svn ci -m 'v??.??' command
#
source common.sh

# Create the zip file
# Files or directories that should not be in the
# public product should be added to the exclude.lst
# file.
#
source makezip.sh $1

# Publish to WordPress Directory?
#
echo
read -p "Publish to WordPress Directory? " -n 1 -r
if [[ "$REPLY" =~ ^[Yy]$ ]]
then

    # Get our version number
    #
    echo
    read -p "What version number are we publishing? " THISVERSION

    # Check ZIP file exists
    #
    if [ ! -f "$ZIP_DIR/$PLUGIN_NAME.zip" ]
    then
        echo "$ZIP_DIR/$PLUGIN_NAME.zip does not exist."
        echo
        exit 0;
    fi

    # Go to the zip directory
    # and remove the unzipped subdir
    #
    if [ ! -d "$PUB_DIR/" ]
    then
        echo "$PUB_DIR/ directory does not exist"
        echo
        exit 0;
    fi
    cd "$PUB_DIR"
    rm -rf "$PUB_DIR/$PLUGIN_NAME"
    unzip "$ZIP_DIR/$PLUGIN_NAME.zip"


    # Go to the repo and update it
    #
    if [ ! -d "$SVN_DIR/trunk" ]
    then
        echo "$SVN_DIR/trunk directory does not exist"
        echo
        exit 0;
    fi
    cd "$SVN_DIR/trunk"
    echo "Copy files from $PUB_DIR/$PLUGIN_NAME/* to trunk at  "
    pwd
    cp -R "$PUB_DIR/$PLUGIN_NAME/"* .
    svn add * --force
    cd ..
    echo "copying trunk to tags/$THISVERSION"
    if [ -d "$SVN_DIR/tags/$THISVERSION" ]
    then
       echo "removing pre-existing tags directory $THISVERSION"
       rm -rf "$SVN_DIR/tags/$THISVERSION"
    fi
    svn cp trunk tags/$THISVERSION
    echo "checking in v$THISVERSION"
    svn ci -m "v$THISVERSION"
fi
echo
exit 1;
